import os, re

TEST_TIME = 10

NIST_LEVELS = [1, 3, 5]

KEM_ALGS = {
    1: ["X25519","P-256","lightsaber", "kyber512", "hqc128", "sidhp434"],
    3: ["X448","P-384","saber", "kyber768", "hqc192", "sidhp610"],
    5: ["P-521","firesaber", "kyber1024", "hqc256", "sidhp751"],
}

SIG_ALGS = {
    1: ["rsa:3072","ed25519","falcon512", "dilithium2", "rainbowIclassic", "sphincsshake256128frobust"],
    3: ["rsa:7680","ed448","dilithium3", "rainbowIIIclassic", "sphincsshake256192frobust"],
    5: ["rsa:15360","falcon1024", "dilithium5", "rainbowVclassic", "sphincsshake256256frobust"],
}

# "p-256", 
# "p-384", 
# "p-521",

with open("raw_results/results_10s_10.csv", "w") as f:
    f.write("nist_level,kem_alg,sig_alg,connections_sec\n")
    for level in NIST_LEVELS:
        print(f"LEVEL {level}")
        for kem_alg in KEM_ALGS[level]:
            for sig_alg in SIG_ALGS[level]:
                command = f"export TEST_TIME={TEST_TIME} && export KEM_ALG={kem_alg} && export SIG_ALG={sig_alg} && bash ./perftest.sh"

                stream = os.popen(command)
                output = stream.read()
                result = re.findall(r"\d+.\d{2} connections\/user sec", output)
                if result:
                    result = result[0]
                    connections_sec = result.split(" ")[0]
                    print(f"{level},{kem_alg},{sig_alg},{connections_sec}\n")
                    f.write(f"{level},{kem_alg},{sig_alg},{connections_sec}\n")
                else:
                    f.write(f"{level},{kem_alg},{sig_alg},{0}\n")
            
