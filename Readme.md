# For the interested reader

This repository contains further information on the practical project **Post-quantum Cryptography in Use: Empirical Analysis of the TLS Handshake Performance** (JEMS ID: **222022**) in particular information on:

- the algorithms of the NIST standardization process
- the security levels of the individual algorithms
- the collection of results
- the test procedures
- the evaluation of the results
  
The corresponding poster can be found at: [POSTER.pdf](./POSTER.pdf)

## Table of contents

1. [Algorithm selection](#algorithm-selection)
2. [Security level classification](#security-level-classification)
3. [Test setup](#test-setup)
4. [Testing procedure](#testing-procedure)
5. [Results](#results)

## Algorithm selection

For information on the mathematical problems of the algorithms watch [Understanding and Explaining Post-Quantum Crypto with Cartoons](https://www.youtube.com/watch?v=6qD-T1gjtKw) for a high-level overview.

### Classical

Analyzing some popular webservers with `nmap -sV --script ssl-enum-ciphers -p 443 amazon.com`

Facebook:

```
|       TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 (secp256r1) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 (secp256r1) - A
|       TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (secp256r1) - A
|       TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA (secp256r1) - C
|       TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA (secp256r1) - C
|       TLS_RSA_WITH_3DES_EDE_CBC_SHA (rsa 2048) – C
```

Google:

```
|       TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|       TLS_RSA_WITH_3DES_EDE_CBC_SHA (rsa 2048) - C
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
```

Apple iCloud:

```
|       TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 (dh 2048) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA256 (rsa 2048) – A
```

Amazon:

```
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
```

### Post-Quantum Cryptography

#### Key Encapsulation Mechanisms (KEM)

| Algorithm        | Group         | Problem    | Chosen |
| ---------------- | ------------- | ---------- | ------ |
| Classic McEliece | code-based    | Goppa-Code |        |
| CRYSTALS-KYBER   | lattice-based | MLWE       | ✅      |
| NTRU             | lattice-based | MLWE, RLWE |        |
| SABER            | lattice-based | MLWR       | ✅      |

alternative candidates:

| Algorithm  | Group         | Problem                                                     | Chosen |
| ---------- | ------------- | ----------------------------------------------------------- | ------ |
| BIKE       | code-based    | QC-MDPC (Quasi-Cyclic Moderate Density Parity-Check)        |        |
| FrodoKEM   | lattice-based | LWE                                                         |        |
| HQC        | code-based    | Syndrome decoding of structure codes (Hamming Quasi-Cyclic) | ✅      |
| NTRU Prime | lattice-based | NTRU                                                        |        |
| SIKE       | ---           | (supersingular) isogeny walk problem                        | ✅      |

### Signature algorithms

| Algorithm          | Group              | Problem    | Chosen |
| ------------------ | ------------------ | ---------- | ------ |
| CRYSTALS-DILITHIUM | lattice-based      | MLWE, MSIS | ✅      |
| FALCON             | lattice-based      | SIS        | ✅      |
| Rainbow            | multivariate-based | UOV        | ✅      |

alternative candidates:

| Algorithm | Group      | Problem                                                                           | Chosen |
| --------- | ---------- | --------------------------------------------------------------------------------- | ------ |
| GeMSS     | ---        | ???                                                                               |        |
| Picnic    | hash-based | hash function security (ROM/QROM), key recovery attacks on the lowMC block cipher |        |
| SPHINCS+  | hash-based | hash-based signatures                                                             | ✅      |

## Security level classification

| NIST-Level | Security description                                        |
| ---------- | ----------------------------------------------------------- |
| 1          | At least as hard to break as AES128 (exhaustive key search) |
| 2          | At least as hard to break as SHA256 (collision search)      |
| 3          | At least as hard to break as AES192 (exhaustive key search) |
| 4          | At least as hard to break as SHA384 (collision search)      |
| 5          | At least as hard to break as AES256 (exhaustive key search) |

### Classical

#### RSA

| Algorithm | NIST-Level |
| --------- | ---------- |
| 3072      | 1          |
| 7680      | 3          |
| 15360     | 5          |

#### ECC

| Algorithm | NIST-Level | Curves   |
| --------- | ---------- | -------- |
| 256-383   | 1          | nistp256 |
| 384-511   | 3          | nistp384 |
| 512+      | 5          | nistp521 |

"faster":

| Algorithm | NIST-Level | Curves          |
| --------- | ---------- | --------------- |
| 253       | 1          | X25519, Ed25519 |
| 448       | 3          | X448, Ed448     |

### PQC-KEMs

#### Saber

| Algorithm      | NIST-Level |
| -------------- | ---------- |
| LightSaber-KEM | 1          |
| Saber-KEM      | 3          |
| FireSaber-KEM  | 5          |

#### Kyber

| Algorithm | NIST-Level |
| --------- | ---------- |
| Kyber512  | 1          |
| Kyber768  | 3          |
| Kyber1024 | 5          |

#### HQC

| Algorithm | NIST-Level |
| --------- | ---------- |
| HQC-128   | 1          |
| HQC-192   | 3          |
| HQC-256   | 5          |

#### SIDH

| Algorithm | NIST-Level |
| --------- | ---------- |
| SIDH-p434 | 1          |
| SIDH-p610 | 3          |
| SIDH-p751 | 5          |

### PQC-Signature

#### Dilithium

| Algorithm  | NIST-Level |
| ---------- | ---------- |
| Dilithium2 | 2          |
| Dilithium3 | 3          |
| Dilithium5 | 5          |

#### Falcon

| Algorithm   | NIST-Level |
| ----------- | ---------- |
| Falcon-512  | 1          |
| Falcon-1024 | 5          |

#### Rainbow

| Algorithm           | NIST-Level |
| ------------------- | ---------- |
| Rainbow-I-Classic   | 1          |
| Rainbow-III-Classic | 3          |
| Rainbow-V-Classic   | 5          |

#### SPHINCS+

| Algorithm                     | NIST-Level |
| ----------------------------- | ---------- |
| SPHINCS+-SHAKE256-128f-robust | 1          |
| SPHINCS+-SHAKE256-192f-robust | 3          |
| SPHINCS+-SHAKE256-256f-robust | 5          |

## Test setup

Intel NUC7PJYH

- Intel® Pentium® Silver J5005 1,5 GHz (up to 2,8 GHz) Quad-Core
- 16GB DDR4 2666MHz Dual-Channel

Software:

- Ubuntu 20.04.3 LTS
- Open Quantum Safe Liboqs (v0.7.1)
- Open Quantum Safe OpenSSL 1.1.1m (v2021-12rc1)

## Testing procedure

### TLS-1.3-Handshake

![TLS-1.3](./figures/TLS-1.3.png)

### Preparation

To run the post-quantum algorithms, liboqs and OpenSSL with appropriate support is required first.
Information about the installation from source:

[Open Quantum Safe - OpenSSL](https://github.com/open-quantum-safe/openssl#quickstart)

The binary `openssl` created in this way can now be executed by specifying the path to it. (Example: `/home/user/openssl/apps/openssl x509...`)

*Note: By default, not all variants of all algorithms are enabled in OpenSSL. More information about activation can be found at: [OpenSSL - Contributing Guide](https://github.com/open-quantum-safe/openssl/wiki/Contributing-Guide#adding-a-key-exchange-algorithm)*

### Pure performance measurements

Using `openssl speed` the performance of the individual algorithms is measured.\
The results were recorded and manually converted into CSV format.\
The converted results can be found in `proc_results/openssl_speed_kem.csv` and `proc_results/openssl_speed_sig.csv`.

### Performing TLS handshakes measurements

For each of the measurements, 5 runs of the `perftest.sh` script were measured. The `test.py` script executes `perftest.sh` and saves the results to a file.

#### perftest.sh

```bash
#!/bin/bash
set -e

export OPENSSL_PATH=/home/eve/openssl/apps/openssl
export PERF_DIR=/home/eve/perf
export OPENSSL_CNF=./openssl.cnf

# Optionally set KEM to one defined in https://github.com/open-quantum-safe/openssl#key-exchange
if [ "x$KEM_ALG" == "x" ]; then
    export KEM_ALG=kyber512
fi

# Optionally set SIG to one defined in https://github.com/open-quantum-safe/openssl#key-exchange
if [ "x$SIG_ALG" == "x" ]; then
    export SIG_ALG=dilithium2
fi

# Optionally set TEST_TIME
if [ "x$TEST_TIME" == "x" ]; then
    export TEST_TIME=10
fi

# ${OPENSSL_PATH} req -x509 -new -newkey ${SIG_ALG} -keyout CA.key -out CA.crt -nodes -subj "/CN=oqstest CA" -days 365 -config ${OPENSSL_CNF}

# Optionally set server certificate alg to one defined in https://github.com/open-quantum-safe/openssl#authentication
# The root CA's signature alg remains as set when building the image
if [ "x$SIG_ALG" != "x" ]; then
    # generate new server CSR using pre-set CA.key & cert
    $OPENSSL_PATH req -new -newkey $SIG_ALG -keyout $PERF_DIR/test/server.key -out $PERF_DIR/test/server.csr -nodes -subj "/CN=localhost" -config ${OPENSSL_CNF}
    if [ $? -ne 0 ]; then
        echo "Error generating keys - aborting."
        exit 1
    fi
    # generate server cert
    $OPENSSL_PATH x509 -req -in $PERF_DIR/test/server.csr -out $PERF_DIR/test/server.crt -CA ca/CA.crt -CAkey ca/CA.key -CAcreateserial -days 365
    if [ $? -ne 0 ]; then
        echo "Error generating cert - aborting."
        exit 1
    fi
fi

echo "Running $0 with SIG_ALG=$SIG_ALG and KEM_ALG=$KEM_ALG"
echo

# Start a TLS1.3 test server based on OpenSSL accepting only the specified KEM_ALG
$OPENSSL_PATH s_server -cert $PERF_DIR/test/server.crt -key $PERF_DIR/test/server.key -curves $KEM_ALG -www -tls1_3 -accept localhost:4433&

# Give server time to come up first:
sleep 1

# Run handshakes for $TEST_TIME seconds
$OPENSSL_PATH s_time -curves $KEM_ALG -connect :4433 -new -time $TEST_TIME -verify 1 | grep connections

PROCESS_ID=`pgrep openssl`
kill $PROCESS_ID
```

Source: [OQS - Demos](https://github.com/open-quantum-safe/oqs-demos)

#### test.py

```python
import os, re

TEST_TIME = 10

NIST_LEVELS = [1, 3, 5]

KEM_ALGS = {
    1: ["X25519","P-256","lightsaber", "kyber512", "hqc128", "sidhp434"],
    3: ["X448","P-384","saber", "kyber768", "hqc192", "sidhp610"],
    5: ["P-521","firesaber", "kyber1024", "hqc256", "sidhp751"],
}

SIG_ALGS = {
    1: ["rsa:3072","ed25519","falcon512", "dilithium2", "rainbowIclassic", "sphincsshake256128frobust"],
    3: ["rsa:7680","ed448","dilithium3", "rainbowIIIclassic", "sphincsshake256192frobust"],
    5: ["rsa:15360","falcon1024", "dilithium5", "rainbowVclassic", "sphincsshake256256frobust"],
}

with open("raw_results/results_10s_10.csv", "w") as f:
    f.write("nist_level,kem_alg,sig_alg,connections_sec\n")
    for level in NIST_LEVELS:
        print(f"LEVEL {level}")
        for kem_alg in KEM_ALGS[level]:
            for sig_alg in SIG_ALGS[level]:
                command = f"export TEST_TIME={TEST_TIME} && export KEM_ALG={kem_alg} && export SIG_ALG={sig_alg} && bash ./perftest.sh"

                stream = os.popen(command)
                output = stream.read()
                result = re.findall(r"\d+.\d{2} connections\/user sec", output)
                if result:
                    result = result[0]
                    connections_sec = result.split(" ")[0]
                    print(f"{level},{kem_alg},{sig_alg},{connections_sec}\n")
                    f.write(f"{level},{kem_alg},{sig_alg},{connections_sec}\n")
                else:
                    f.write(f"{level},{kem_alg},{sig_alg},{0}\n")

```

The results are saved as CSV:

| nist_level | kem_alg | sig_alg   | connections_sec |
| ---------- | ------- | --------- | --------------- |
| 1          | X25519  | rsa:3072  | 1275.49         |
| 1          | X25519  | ed25519   | 1085.42         |
| 1          | X25519  | falcon512 | 1473.68         |
...

## Results

After 5 runs of `test.py` the results are accumulated and evaluated in the notebook `evaluation.ipynb`.

There the results of `openssl speed` are also graphically processed.

### Signature algorithms

| level | algorithm                         | sign_s  | verify_s |
| ----- | --------------------------------- | ------- | -------- |
| 0     | rsa:512                           | 12697.6 | 167032.5 |
| 0     | rsa:1024                          | 3717.6  | 59686.4  |
| 0     | rsa:2048                          | 506.1   | 17425.9  |
| 1     | rsa:3072                          | 160.6   | 8094.1   |
| 0     | rsa:4096                          | 71.7    | 4652.0   |
| 3     | rsa:7680                          | 9.1     | 1348.4   |
| 5     | rsa:15360                         | 1.5     | 343.8    |
| 1     | ed25519                           | 12662.3 | 3799.5   |
| 3     | ed448                             | 1771.8  | 980.3    |
| 1     | falcon512                         | 120.3   | 10435.7  |
| 0     | p256_falcon512                    | 119.4   | 3561.2   |
| 0     | rsa3072_falcon512                 | 68.6    | 4489.7   |
| 5     | falcon1024                        | 55.0    | 5142.2   |
| 0     | p521_falcon1024                   | 44.1    | 269.4    |
| 1     | dilithium2                        | 1126.8  | 5078.0   |
| 0     | p256_dilithium2                   | 1049.8  | 2607.0   |
| 0     | rsa3072_dilithium2                | 139.9   | 3071.6   |
| 3     | dilithium3                        | 696.3   | 3170.8   |
| 0     | p384_dilithium3                   | 308.7   | 552.2    |
| 5     | dilithium5                        | 583.1   | 1977.2   |
| 0     | p521_dilithium5                   | 161.1   | 249.9    |
| 0     | dilithium2_aes                    | 861.7   | 2739.0   |
| 0     | p256_dilithium2_aes               | 800.5   | 1815.1   |
| 0     | rsa3072_dilithium2_aes            | 134.9   | 2025.6   |
| 0     | dilithium3_aes                    | 507.8   | 1590.4   |
| 0     | p384_dilithium3_aes               | 267.3   | 475.4    |
| 0     | dilithium5_aes                    | 397.9   | 918.0    |
| 0     | p521_dilithium5_aes               | 141.1   | 217.1    |
| 0     | picnicl1full                      | 257.7   | 339.2    |
| 0     | p256_picnicl1full                 | 253.7   | 319.7    |
| 0     | rsa3072_picnicl1full              | 98.5    | 324.0    |
| 0     | picnic3l1                         | 52.3    | 65.0     |
| 0     | p256_picnic3l1                    | 52.3    | 64.1     |
| 0     | rsa3072_picnic3l1                 | 39.6    | 64.4     |
| 1     | rainbowIclassic                   | 294.2   | 277.6    |
| 0     | p256_rainbowIclassic              | 289.1   | 264.8    |
| 0     | rsa3072_rainbowIclassic           | 103.9   | 268.2    |
| 3     | rainbowIIIclassic                 | 32.9    | 31.5     |
| 0     | p384_rainbowIIIclassic            | 31.0    | 29.9     |
| 5     | rainbowVclassic                   | 14.9    | 14.4     |
| 0     | p521_rainbowVclassic              | 12.7    | 13.7     |
| 0     | sphincsharaka128frobust           | 61.9    | 992.9    |
| 0     | p256_sphincsharaka128frobust      | 61.6    | 879.2    |
| 0     | rsa3072_sphincsharaka128frobust   | 44.6    | 901.9    |
| 0     | sphincssha256128frobust           | 11.5    | 190.4    |
| 0     | p256_sphincssha256128frobust      | 11.5    | 192.1    |
| 0     | rsa3072_sphincssha256128frobust   | 10.7    | 187.7    |
| 1     | sphincsshake256128frobust         | 4.6     | 82.3     |
| 0     | p256_sphincsshake256128frobust    | 4.6     | 82.4     |
| 0     | rsa3072_sphincsshake256128frobust | 4.5     | 79.2     |
| 3     | sphincsshake256192frobust         | 2.9     | 55.0     |
| 0     | p384_sphincsshake256192frobust    | 2.9     | 50.2     |
| 5     | sphincsshake256256frobust         | 1.5     | 54.1     |
| 0     | p521_sphincsshake256256frobust    | 1.4     | 44.7     |

![Security level 1](./figures/sig_results_l1.png)
![Security level 3](./figures/sig_results_l3.png)
![Security level 5](./figures/sig_results_l5.png)

### Key Encapsulation mechanisms

| level | algorithm        | keygen_s | encap_s | decap_s |
| ----- | ---------------- | -------- | ------- | ------- |
| 0     | frodo640aes      | 1368.5   | 960.4   | 996.9   |
| 0     | frodo640shake    | 236.6    | 210.9   | 212.7   |
| 0     | frodo976aes      | 622.6    | 469.8   | 489.1   |
| 0     | frodo976shake    | 105.4    | 94.9    | 95.5    |
| 0     | frodo1344aes     | 350.9    | 267.0   | 273.6   |
| 0     | frodo1344shake   | 58.2     | 52.7    | 53.1    |
| 1     | lightsaber       | 17349.7  | 14872.2 | 14327.1 |
| 3     | saber            | 10042.2  | 8580.1  | 8078.3  |
| 5     | firesaber        | 6317.9   | 5567.2  | 4911.2  |
| 1     | kyber512         | 13154.3  | 10071.7 | 8639.6  |
| 3     | kyber768         | 8263.0   | 6601.4  | 5729.1  |
| 5     | kyber1024        | 5477.6   | 4600.4  | 4045.6  |
| 0     | ntru_hps2048509  | 448.5    | 10574.3 | 5643.3  |
| 0     | ntru_hps2048677  | 271.1    | 7195.6  | 3708.9  |
| 0     | ntru_hps4096821  | 196.6    | 6008.9  | 3120.9  |
| 0     | ntru_hps40961229 | 95.8     | 3783.4  | 1988.9  |
| 0     | ntru_hrss701     | 256.3    | 9541.7  | 3552.1  |
| 0     | ntru_hrss1373    | 76.5     | 4103.0  | 1436.9  |
| 1     | hqc128           | 4629.9   | 2258.8  | 1518.7  |
| 3     | hqc192           | 1796.0   | 845.8   | 584.5   |
| 5     | hqc256           | 973.4    | 452.9   | 311.3   |
| 1     | sidhp434         | 64.1     | 31.2    | 78.9    |
| 0     | sidhp503         | 140.0    | 68.7    | 171.5   |
| 3     | sidhp610         | 19.4     | 10.4    | 23.2    |
| 5     | sidhp751         | 46.0     | 22.0    | 55.9    |
| 0     | sikep434         | 57.5     | 35.3    | 33.0    |
| 0     | sikep503         | 126.7    | 77.1    | 72.3    |
| 0     | sikep610         | 19.3     | 10.5    | 10.4    |
| 0     | sikep751         | 40.7     | 25.2    | 23.5    |
| 0     | bikel1           | 1047.9   | 4480.2  | 208.2   |
| 0     | bikel3           | 345.2    | 1647.8  | 75.0    |
| 0     | kyber90s512      | 8951.4   | 7533.1  | 6498.2  |
| 0     | kyber90s768      | 5146.0   | 4524.2  | 3994.6  |
| 0     | kyber90s1024     | 3229.7   | 2935.1  | 2644.2  |
| 0     | ntrulpr653       | 6125.7   | 3819.9  | 2834.0  |
| 0     | ntrulpr761       | 5118.8   | 3138.9  | 2311.9  |
| 0     | ntrulpr857       | 4344.8   | 2635.1  | 1924.1  |
| 0     | ntrulpr1277      | 2402.0   | 1401.0  | 1007.9  |
| 0     | sntrup653        | 151.2    | 6099.0  | 447.1   |
| 0     | sntrup761        | 115.1    | 5038.9  | 333.4   |
| 0     | sntrup857        | 88.5     | 4236.5  | 264.6   |
| 0     | sntrup1277       | 42.2     | 2341.2  | 120.6   |

![Security level 1](./figures/kem_results_l1.png)
![Security level 3](./figures/kem_results_l3.png)
![Security level 5](./figures/kem_results_l5.png)

### TLS-1.3-Handshakes

| nist_level | kem_alg    | sig_alg                   | mean               | std                  |
| ---------- | ---------- | ------------------------- | ------------------ | -------------------- |
| 1          | X25519     | rsa:3072                  | 1300.124           | 27.364049846468266   |
| 1          | X25519     | ed25519                   | 1092.282           | 8.459591952334296    |
| 1          | X25519     | falcon512                 | 1454.608           | 32.55277155635137    |
| 1          | X25519     | dilithium2                | 1150.4279999999999 | 18.24407454490361    |
| 1          | X25519     | rainbowIclassic           | 146.22             | 1.0317751693077277   |
| 1          | X25519     | sphincsshake256128frobust | 79.12              | 0.5                  |
| 1          | P-256      | rsa:3072                  | 1237.5120000000002 | 11.810602694189683   |
| 1          | P-256      | ed25519                   | 953.448            | 10.635484756229992   |
| 1          | P-256      | falcon512                 | 1263.1979999999999 | 40.529829706032565   |
| 1          | P-256      | dilithium2                | 1089.0459999999998 | 4.4052722957837585   |
| 1          | P-256      | rainbowIclassic           | 144.114            | 1.1840878345798518   |
| 1          | P-256      | sphincsshake256128frobust | 78.33200000000001  | 1.3492131039980297   |
| 1          | lightsaber | rsa:3072                  | 1404.172           | 29.261345423613026   |
| 1          | lightsaber | ed25519                   | 1225.2240000000002 | 3.5454342470281297   |
| 1          | lightsaber | falcon512                 | 1438.3500000000001 | 44.13273886810106    |
| 1          | lightsaber | dilithium2                | 1215.058           | 20.465289052441936   |
| 1          | lightsaber | rainbowIclassic           | 145.982            | 1.1119604309506728   |
| 1          | lightsaber | sphincsshake256128frobust | 79.382             | 1.1314663052870824   |
| 1          | kyber512   | rsa:3072                  | 1251.0040000000001 | 38.34914059010971    |
| 1          | kyber512   | ed25519                   | 1067.6699999999998 | 9.695070912582302    |
| 1          | kyber512   | falcon512                 | 1314.33            | 31.91668341165793    |
| 1          | kyber512   | dilithium2                | 1108.704           | 9.093867384122136    |
| 1          | kyber512   | rainbowIclassic           | 144.882            | 1.4562609656239511   |
| 1          | kyber512   | sphincsshake256128frobust | 79.902             | 1.53275438345483     |
| 1          | hqc128     | rsa:3072                  | 842.5560000000002  | 111.71456424298493   |
| 1          | hqc128     | ed25519                   | 610.592            | 5.032325108734533    |
| 1          | hqc128     | falcon512                 | 659.918            | 5.228048966870901    |
| 1          | hqc128     | dilithium2                | 591.578            | 3.269870945465588    |
| 1          | hqc128     | rainbowIclassic           | 135.464            | 1.2791497175858666   |
| 1          | hqc128     | sphincsshake256128frobust | 73.316             | 0.42800000000000293  |
| 1          | sidhp434   | rsa:3072                  | 33.966             | 0.1416474496770049   |
| 1          | sidhp434   | ed25519                   | 33.874             | 0.05238320341483539  |
| 1          | sidhp434   | falcon512                 | 33.932             | 0.14441606558828382  |
| 1          | sidhp434   | dilithium2                | 33.968             | 0.1225397894563251   |
| 1          | sidhp434   | rainbowIclassic           | 29.264             | 0.06621178142898661  |
| 1          | sidhp434   | sphincsshake256128frobust | 24.288             | 0.204098015668943    |
| 3          | X448       | rsa:7680                  | 342.81399999999996 | 12.762812542696066   |
| 3          | X448       | ed448                     | 263.718            | 1.5202683973562099   |
| 3          | X448       | dilithium3                | 409.98999999999995 | 5.377214892488479    |
| 3          | X448       | rainbowIIIclassic         | 24.278             | 0.18988417522268694  |
| 3          | X448       | sphincsshake256192frobust | 49.032             | 0.7741421058177894   |
| 3          | P-384      | rsa:7680                  | 198.61399999999998 | 1.131876318331651    |
| 3          | P-384      | ed448                     | 142.252            | 0.5568985544962318   |
| 3          | P-384      | dilithium3                | 196.57799999999997 | 1.8944065033672122   |
| 3          | P-384      | rainbowIIIclassic         | 23.036             | 0.08662563131083179  |
| 3          | P-384      | sphincsshake256192frobust | 45.59              | 0.2799999999999983   |
| 3          | saber      | rsa:7680                  | 618.182            | 41.05371232909393    |
| 3          | saber      | ed448                     | 520.42             | 7.0054464525824365   |
| 3          | saber      | dilithium3                | 957.6299999999999  | 4.327664497162397    |
| 3          | saber      | rainbowIIIclassic         | 24.982             | 0.07194442299441951  |
| 3          | saber      | sphincsshake256192frobust | 52.492             | 0.5257908329364432   |
| 3          | kyber768   | rsa:7680                  | 570.93             | 39.3052429072764     |
| 3          | kyber768   | ed448                     | 496.68199999999996 | 4.5390457146849785   |
| 3          | kyber768   | dilithium3                | 866.184            | 13.923806376131518   |
| 3          | kyber768   | rainbowIIIclassic         | 24.947999999999997 | 0.14469277798148727  |
| 3          | kyber768   | sphincsshake256192frobust | 52.722             | 0.3640000000000015   |
| 3          | hqc192     | rsa:7680                  | 261.716            | 3.9214364714986822   |
| 3          | hqc192     | ed448                     | 247.36399999999998 | 1.224901628703303    |
| 3          | hqc192     | dilithium3                | 288.38             | 2.0163233867611647   |
| 3          | hqc192     | rainbowIIIclassic         | 23.56              | 0.1902629759044042   |
| 3          | hqc192     | sphincsshake256192frobust | 47.282000000000004 | 1.0981147481024      |
| 3          | sidhp610   | rsa:7680                  | 10.123999999999999 | 0.02576819745345042  |
| 3          | sidhp610   | ed448                     | 10.325999999999999 | 0.024166091947189158 |
| 3          | sidhp610   | dilithium3                | 10.373999999999999 | 0.02653299832284333  |
| 3          | sidhp610   | rainbowIIIclassic         | 7.372000000000002  | 0.017204650534085403 |
| 3          | sidhp610   | sphincsshake256192frobust | 8.802000000000001  | 0.02993325909419165  |
| 5          | P-521      | rsa:15360                 | 75.022             | 1.2635410559218068   |
| 5          | P-521      | falcon1024                | 99.80000000000001  | 0.1633401359127634   |
| 5          | P-521      | dilithium5                | 92.03599999999999  | 0.16668533228811838  |
| 5          | P-521      | rainbowVclassic           | 10.524000000000001 | 0.07172168430816465  |
| 5          | P-521      | sphincsshake256256frobust | 36.414             | 0.35200000000000103  |
| 5          | firesaber  | rsa:15360                 | 231.43             | 16.033892852330034   |
| 5          | firesaber  | falcon1024                | 917.622            | 36.76236630033491    |
| 5          | firesaber  | dilithium5                | 691.1940000000001  | 9.863746955391747    |
| 5          | firesaber  | rainbowVclassic           | 11.608             | 0.04833218389437852  |
| 5          | firesaber  | sphincsshake256256frobust | 51.331999999999994 | 1.9230642214965148   |
| 5          | kyber1024  | rsa:15360                 | 224.288            | 13.995159734708286   |
| 5          | kyber1024  | falcon1024                | 889.692            | 19.446021084016127   |
| 5          | kyber1024  | dilithium5                | 638.934            | 1.9070039328748052   |
| 5          | kyber1024  | rainbowVclassic           | 11.596             | 0.04317406628984596  |
| 5          | kyber1024  | sphincsshake256256frobust | 52.31999999999999  | 1.337669615413312    |
| 5          | hqc256     | rsa:15360                 | 115.178            | 5.195191623029895    |
| 5          | hqc256     | falcon1024                | 194.67000000000002 | 3.0035911838996996   |
| 5          | hqc256     | dilithium5                | 169.454            | 0.634211321248687    |
| 5          | hqc256     | rainbowVclassic           | 11.075999999999999 | 0.026532998322843115 |
| 5          | hqc256     | sphincsshake256256frobust | 42.622             | 0.47599999999999903  |
| 5          | sidhp751   | rsa:15360                 | 22.298             | 0.2824464551025555   |
| 5          | sidhp751   | falcon1024                | 24.304000000000002 | 0.05953150426454824  |
| 5          | sidhp751   | dilithium5                | 24.36              | 0.03162277660168424  |
| 5          | sidhp751   | rainbowVclassic           | 7.958              | 0.027129319932500836 |
| 5          | sidhp751   | sphincsshake256256frobust | 16.95              | 0.09797958971132678  |

![Security level 1](./figures/results_l1.png)
![Security level 3](./figures/results_l3.png)
![Security level 5](./figures/results_l5.png)
