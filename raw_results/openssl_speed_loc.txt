OpenSSL 1.1.1m  14 Dec 2021, Open Quantum Safe 2021-12 snapshot rc1
built on: Mon Jan  3 10:58:08 2022 UTC
options:bn(64,64) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr) -frodo640aes,frodo640shake,frodo976aes,frodo976shake,frodo1344aes,frodo1344shake,kyber512,kyber768,kyber1024,ntru_hps2048509,ntru_hps2048677,ntru_hps4096821,ntru_hps40961229,ntru_hrss701,ntru_hrss1373,lightsaber,saber,firesaber,sidhp434,sidhp503,sidhp610,sidhp751,sikep434,sikep503,sikep610,sikep751,bikel1,bikel3,kyber90s512,kyber90s768,kyber90s1024,hqc128,hqc192,hqc256,ntrulpr653,ntrulpr761,ntrulpr857,ntrulpr1277,sntrup653,sntrup761,sntrup857,sntrup1277 -dilithium2,p256_dilithium2,rsa3072_dilithium2,dilithium3,p384_dilithium3,dilithium5,p521_dilithium5,dilithium2_aes,p256_dilithium2_aes,rsa3072_dilithium2_aes,dilithium3_aes,p384_dilithium3_aes,dilithium5_aes,p521_dilithium5_aes,falcon512,p256_falcon512,rsa3072_falcon512,falcon1024,p521_falcon1024,picnicl1full,p256_picnicl1full,rsa3072_picnicl1full,picnic3l1,p256_picnic3l1,rsa3072_picnic3l1,rainbowIclassic,p256_rainbowIclassic,rsa3072_rainbowIclassic,rainbowIIIclassic,p384_rainbowIIIclassic,rainbowVclassic,p521_rainbowVclassic,sphincsharaka128frobust,p256_sphincsharaka128frobust,rsa3072_sphincsharaka128frobust,sphincssha256128frobust,p256_sphincssha256128frobust,rsa3072_sphincssha256128frobust,sphincsshake256128frobust,p256_sphincsshake256128frobust,rsa3072_sphincsshake256128frobust,sphincsshake256192frobust,p384_sphincsshake256192frobust,sphincsshake256256frobust,p521_sphincsshake256256frobust 
compiler: gcc -fPIC -pthread -m64 -Ioqs/include -Wa,--noexecstack -Wall -O3 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG
The 'numbers' are in 1000s of bytes per second processed.
type             16 bytes     64 bytes    256 bytes   1024 bytes   8192 bytes  16384 bytes 102400 bytes
md2                  0.00         0.00         0.00         0.00         0.00         0.00         0.00 
mdc2             10670.65k    11733.76k    12105.90k    12164.44k    12227.93k    12184.23k    12219.73k
md4              47098.15k   155021.65k   403917.48k   674585.26k   841231.02k   855299.41k   856371.20k
md5              83786.86k   208374.36k   386425.69k   490927.79k   536494.08k   534451.54k   526745.60k
hmac(md5)        32647.47k   105432.02k   265825.71k   428531.03k   518690.13k   534495.23k   529681.07k
sha1            172832.42k   495319.62k  1045331.03k  1447863.30k  1625462.10k  1651556.35k  1675127.47k
rmd160           28733.42k    73842.24k   142266.79k   185254.23k   202274.13k   203745.96k   203878.40k
rc4             382026.72k   473273.98k   478570.41k   464591.87k   459748.69k   459467.43k   457957.46k
des cbc          55424.36k    57666.92k    58071.30k    58120.87k    58302.46k    58086.74k    57890.13k
des ede3         20282.35k    20872.11k    20981.42k    21007.70k    20990.63k    21009.75k    21026.13k
idea cbc         56598.76k    59008.28k    59581.78k    59548.67k    59757.91k    59774.29k    59630.93k
seed cbc         70833.06k    73212.57k    73420.89k    73472.34k    73348.44k    73591.47k    73523.20k
rc2 cbc          33467.66k    34120.85k    34283.35k    34362.71k    34346.33k    34417.32k    34372.27k
rc5-32/12 cbc        0.00         0.00         0.00         0.00         0.00         0.00         0.00 
blowfish cbc    105087.24k   112691.56k   114263.55k   115171.33k   115807.57k   115146.75k   115200.00k
cast cbc         96048.17k   101522.43k   102548.74k   103073.45k   103459.50k   103241.05k   102877.87k
aes-128 cbc     115850.95k   121981.82k   123474.86k   124183.21k   124504.75k   122776.23k   121412.27k
aes-192 cbc      99268.73k   104220.48k   104996.35k   104841.56k   105070.59k   104174.93k   103526.40k
aes-256 cbc      87128.98k    91165.08k    91323.48k    91739.14k    91693.06k    90789.21k    90419.20k
camellia-128 cbc    59747.54k    94829.40k   110717.27k   115960.15k   117462.36k   117631.66k   117657.60k
camellia-192 cbc    51675.88k    75171.50k    84576.26k    87536.98k    88511.83k    88440.83k    88644.27k
camellia-256 cbc    51624.16k    75167.55k    84509.61k    87440.04k    88536.41k    88550.06k    88405.33k
sha256          111488.54k   272885.23k   498043.82k   627040.94k   678141.95k   681028.27k   685977.60k
sha512           26677.17k   106374.36k   161608.62k   223968.26k   251704.66k   255120.73k   255146.67k
whirlpool        21646.97k    45387.43k    75571.88k    90825.39k    96452.61k    96818.52k    97041.07k
aes-128 ige     108986.14k   117117.85k   119206.14k   119636.31k   120026.45k   119296.34k   119466.67k
aes-192 ige      94151.72k   100630.81k   102007.04k   102984.26k   102842.37k   102247.08k   101956.27k
aes-256 ige      83121.99k    88004.99k    89361.32k    89690.45k    89896.28k    89505.79k    89098.21k
ghash           907397.43k  1737699.84k  2287557.46k  2493865.30k  2553391.79k  2565215.57k  2568908.80k
rand              4600.78k    17559.88k    68545.19k   246070.49k  1246706.29k  1698472.59k  2147489.32k
                  sign    verify    sign/s verify/s
rsa  512 bits 0.000079s 0.000006s  12697.6 167032.5
rsa 1024 bits 0.000269s 0.000017s   3717.6  59686.4
rsa 2048 bits 0.001976s 0.000057s    506.1  17425.9
rsa 3072 bits 0.006227s 0.000124s    160.6   8094.1
rsa 4096 bits 0.013947s 0.000215s     71.7   4652.0
rsa 7680 bits 0.110330s 0.000742s      9.1   1348.4
rsa 15360 bits 0.686000s 0.002909s      1.5    343.8
                  sign    verify    sign/s verify/s
dsa  512 bits 0.000128s 0.000086s   7838.7  11677.6
dsa 1024 bits 0.000265s 0.000218s   3775.9   4592.5
dsa 2048 bits 0.000793s 0.000729s   1261.3   1372.6
                              sign    verify    sign/s verify/s
 160 bits ecdsa (secp160r1)   0.0004s   0.0004s   2636.5   2782.1
 192 bits ecdsa (nistp192)   0.0005s   0.0004s   2179.3   2425.2
 224 bits ecdsa (nistp224)   0.0007s   0.0006s   1471.3   1680.3
 256 bits ecdsa (nistp256)   0.0001s   0.0002s  17436.0   5500.7
 384 bits ecdsa (nistp384)   0.0018s   0.0014s    552.3    706.8
 521 bits ecdsa (nistp521)   0.0045s   0.0035s    223.3    288.7
 163 bits ecdsa (nistk163)   0.0004s   0.0008s   2394.5   1208.6
 233 bits ecdsa (nistk233)   0.0006s   0.0011s   1724.2    872.0
 283 bits ecdsa (nistk283)   0.0010s   0.0019s   1033.3    524.8
 409 bits ecdsa (nistk409)   0.0017s   0.0033s    594.4    304.4
 571 bits ecdsa (nistk571)   0.0036s   0.0071s    275.0    140.6
 163 bits ecdsa (nistb163)   0.0004s   0.0009s   2319.7   1175.4
 233 bits ecdsa (nistb233)   0.0006s   0.0012s   1694.0    855.8
 283 bits ecdsa (nistb283)   0.0010s   0.0020s    988.0    500.3
 409 bits ecdsa (nistb409)   0.0018s   0.0035s    566.0    287.3
 571 bits ecdsa (nistb571)   0.0039s   0.0076s    256.3    131.4
 256 bits ecdsa (brainpoolP256r1)   0.0008s   0.0007s   1332.8   1454.2
 256 bits ecdsa (brainpoolP256t1)   0.0008s   0.0007s   1317.9   1477.0
 384 bits ecdsa (brainpoolP384r1)   0.0018s   0.0016s    554.8    642.1
 384 bits ecdsa (brainpoolP384t1)   0.0018s   0.0014s    562.1    700.3
 512 bits ecdsa (brainpoolP512r1)   0.0033s   0.0026s    305.8    380.6
 512 bits ecdsa (brainpoolP512t1)   0.0032s   0.0025s    307.9    408.1
                              op      op/s
 160 bits ecdh (secp160r1)   0.0004s   2808.2
 192 bits ecdh (nistp192)   0.0004s   2303.8
 224 bits ecdh (nistp224)   0.0006s   1559.7
 256 bits ecdh (nistp256)   0.0001s   7191.5
 384 bits ecdh (nistp384)   0.0017s    580.0
 521 bits ecdh (nistp521)   0.0043s    235.0
 163 bits ecdh (nistk163)   0.0004s   2502.1
 233 bits ecdh (nistk233)   0.0005s   1821.3
 283 bits ecdh (nistk283)   0.0009s   1089.5
 409 bits ecdh (nistk409)   0.0016s    635.9
 571 bits ecdh (nistk571)   0.0034s    293.0
 163 bits ecdh (nistb163)   0.0004s   2433.5
 233 bits ecdh (nistb233)   0.0006s   1789.6
 283 bits ecdh (nistb283)   0.0010s   1035.6
 409 bits ecdh (nistb409)   0.0017s    599.0
 571 bits ecdh (nistb571)   0.0037s    273.0
 256 bits ecdh (brainpoolP256r1)   0.0007s   1392.1
 256 bits ecdh (brainpoolP256t1)   0.0007s   1398.3
 384 bits ecdh (brainpoolP384r1)   0.0017s    583.3
 384 bits ecdh (brainpoolP384t1)   0.0017s    590.7
 512 bits ecdh (brainpoolP512r1)   0.0031s    320.1
 512 bits ecdh (brainpoolP512t1)   0.0031s    323.8
 253 bits ecdh (X25519)   0.0001s  11257.1
 448 bits ecdh (X448)   0.0009s   1118.5
                              sign    verify    sign/s verify/s
 253 bits EdDSA (Ed25519)   0.0001s   0.0003s  12662.3   3799.5
 456 bits EdDSA (Ed448)   0.0006s   0.0010s   1771.8    980.3
                              keygen/s      encap/s      decap/s
                  frodo640aes   1368.5        960.4        996.9
                frodo640shake    236.6        210.9        212.7
                  frodo976aes    622.6        469.8        489.1
                frodo976shake    105.4         94.9         95.5
                 frodo1344aes    350.9        267.0        273.6
               frodo1344shake     58.2         52.7         53.1
                     kyber512  13154.3      10071.7       8639.6
                     kyber768   8263.0       6601.4       5729.1
                    kyber1024   5477.6       4600.4       4045.6
              ntru_hps2048509    448.5      10574.3       5643.3
              ntru_hps2048677    271.1       7195.6       3708.9
              ntru_hps4096821    196.6       6008.9       3120.9
             ntru_hps40961229     95.8       3783.4       1988.9
                 ntru_hrss701    256.3       9541.7       3552.1
                ntru_hrss1373     76.5       4103.0       1436.9
                   lightsaber  17349.7      14872.2      14327.1
                        saber  10042.2       8580.1       8078.3
                    firesaber   6317.9       5567.2       4911.2
                     sidhp434     64.1         31.2         78.9
                     sidhp503    140.0         68.7        171.5
                     sidhp610     19.4         10.4         23.2
                     sidhp751     46.0         22.0         55.9
                     sikep434     57.5         35.3         33.0
                     sikep503    126.7         77.1         72.3
                     sikep610     19.3         10.5         10.4
                     sikep751     40.7         25.2         23.5
                       bikel1   1047.9       4480.2        208.2
                       bikel3    345.2       1647.8         75.0
                  kyber90s512   8951.4       7533.1       6498.2
                  kyber90s768   5146.0       4524.2       3994.6
                 kyber90s1024   3229.7       2935.1       2644.2
                       hqc128   4629.9       2258.8       1518.7
                       hqc192   1796.0        845.8        584.5
                       hqc256    973.4        452.9        311.3
                   ntrulpr653   6125.7       3819.9       2834.0
                   ntrulpr761   5118.8       3138.9       2311.9
                   ntrulpr857   4344.8       2635.1       1924.1
                  ntrulpr1277   2402.0       1401.0       1007.9
                    sntrup653    151.2       6099.0        447.1
                    sntrup761    115.1       5038.9        333.4
                    sntrup857     88.5       4236.5        264.6
                   sntrup1277     42.2       2341.2        120.6
                                    sign    verify   sign/s  verify/s
                   dilithium2:   0.0009s   0.0002s   1126.8   5078.0
              p256_dilithium2:   0.0010s   0.0004s   1049.8   2607.0
           rsa3072_dilithium2:   0.0071s   0.0003s    139.9   3071.6
                   dilithium3:   0.0014s   0.0003s    696.3   3170.8
              p384_dilithium3:   0.0032s   0.0018s    308.7    552.2
                   dilithium5:   0.0017s   0.0005s    583.1   1977.2
              p521_dilithium5:   0.0062s   0.0040s    161.1    249.9
               dilithium2_aes:   0.0012s   0.0004s    861.7   2739.0
          p256_dilithium2_aes:   0.0012s   0.0006s    800.5   1815.1
       rsa3072_dilithium2_aes:   0.0074s   0.0005s    134.9   2025.6
               dilithium3_aes:   0.0020s   0.0006s    507.8   1590.4
          p384_dilithium3_aes:   0.0037s   0.0021s    267.3    475.4
               dilithium5_aes:   0.0025s   0.0011s    397.9    918.0
          p521_dilithium5_aes:   0.0071s   0.0046s    141.1    217.1
                    falcon512:   0.0083s   0.0001s    120.3  10435.7
               p256_falcon512:   0.0084s   0.0003s    119.4   3561.2
            rsa3072_falcon512:   0.0146s   0.0002s     68.6   4489.7
                   falcon1024:   0.0182s   0.0002s     55.0   5142.2
              p521_falcon1024:   0.0227s   0.0037s     44.1    269.4
                 picnicl1full:   0.0039s   0.0029s    257.7    339.2
            p256_picnicl1full:   0.0039s   0.0031s    253.7    319.7
         rsa3072_picnicl1full:   0.0102s   0.0031s     98.5    324.0
                    picnic3l1:   0.0191s   0.0154s     52.3     65.0
               p256_picnic3l1:   0.0191s   0.0156s     52.3     64.1
            rsa3072_picnic3l1:   0.0253s   0.0155s     39.6     64.4
              rainbowIclassic:   0.0034s   0.0036s    294.2    277.6
         p256_rainbowIclassic:   0.0035s   0.0038s    289.1    264.8
      rsa3072_rainbowIclassic:   0.0096s   0.0037s    103.9    268.2
            rainbowIIIclassic:   0.0304s   0.0318s     32.9     31.5
       p384_rainbowIIIclassic:   0.0323s   0.0334s     31.0     29.9
              rainbowVclassic:   0.0672s   0.0692s     14.9     14.4
         p521_rainbowVclassic:   0.0787s   0.0729s     12.7     13.7
      sphincsharaka128frobust:   0.0161s   0.0010s     61.9    992.9
 p256_sphincsharaka128frobust:   0.0162s   0.0011s     61.6    879.2
rsa3072_sphincsharaka128frobust:   0.0224s   0.0011s     44.6    901.9
      sphincssha256128frobust:   0.0872s   0.0053s     11.5    190.4
 p256_sphincssha256128frobust:   0.0871s   0.0052s     11.5    192.1
rsa3072_sphincssha256128frobust:   0.0932s   0.0053s     10.7    187.7
    sphincsshake256128frobust:   0.2160s   0.0121s      4.6     82.3
p256_sphincsshake256128frobust:   0.2166s   0.0121s      4.6     82.4
rsa3072_sphincsshake256128frobust:   0.2220s   0.0126s      4.5     79.2
    sphincsshake256192frobust:   0.3440s   0.0182s      2.9     55.0
p384_sphincsshake256192frobust:   0.3437s   0.0199s      2.9     50.2
    sphincsshake256256frobust:   0.6860s   0.0185s      1.5     54.1
p521_sphincsshake256256frobust:   0.6953s   0.0224s      1.4     44.7
