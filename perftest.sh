#!/bin/bash
set -e

# See https://github.com/open-quantum-safe/oqs-demos for more information
export OPENSSL_PATH=/home/eve/openssl/apps/openssl
export PERF_DIR=/home/eve/perf
export OPENSSL_CNF=./openssl.cnf

# Optionally set KEM to one defined in https://github.com/open-quantum-safe/openssl#key-exchange
if [ "x$KEM_ALG" == "x" ]; then
    export KEM_ALG=kyber512
fi

# Optionally set SIG to one defined in https://github.com/open-quantum-safe/openssl#key-exchange
if [ "x$SIG_ALG" == "x" ]; then
    export SIG_ALG=dilithium2
fi

# Optionally set TEST_TIME
if [ "x$TEST_TIME" == "x" ]; then
    export TEST_TIME=10
fi

# ${OPENSSL_PATH} req -x509 -new -newkey ${SIG_ALG} -keyout CA.key -out CA.crt -nodes -subj "/CN=oqstest CA" -days 365 -config ${OPENSSL_CNF}

# Optionally set server certificate alg to one defined in https://github.com/open-quantum-safe/openssl#authentication
# The root CA's signature alg remains as set when building the image
if [ "x$SIG_ALG" != "x" ]; then
    # generate new server CSR using pre-set CA.key & cert
    $OPENSSL_PATH req -new -newkey $SIG_ALG -keyout $PERF_DIR/test/server.key -out $PERF_DIR/test/server.csr -nodes -subj "/CN=localhost" -config ${OPENSSL_CNF}
    if [ $? -ne 0 ]; then
        echo "Error generating keys - aborting."
        exit 1
    fi
    # generate server cert
    $OPENSSL_PATH x509 -req -in $PERF_DIR/test/server.csr -out $PERF_DIR/test/server.crt -CA ca/CA.crt -CAkey ca/CA.key -CAcreateserial -days 365
    if [ $? -ne 0 ]; then
        echo "Error generating cert - aborting."
        exit 1
    fi
fi

echo "Running $0 with SIG_ALG=$SIG_ALG and KEM_ALG=$KEM_ALG"
echo

# Start a TLS1.3 test server based on OpenSSL accepting only the specified KEM_ALG
$OPENSSL_PATH s_server -cert $PERF_DIR/test/server.crt -key $PERF_DIR/test/server.key -curves $KEM_ALG -www -tls1_3 -accept localhost:4433&

# Give server time to come up first:
sleep 1

# Run handshakes for $TEST_TIME seconds
$OPENSSL_PATH s_time -curves $KEM_ALG -connect :4433 -new -time $TEST_TIME -verify 1 | grep connections

PROCESS_ID=`pgrep openssl`
kill $PROCESS_ID